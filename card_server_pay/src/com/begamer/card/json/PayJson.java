package com.begamer.card.json;


public class PayJson
{
	private String userId;//用户通行证账号
	private String userNumId;//用户通行证数字ID
	private String gameId;//游戏ID
	private long reqtime;//消费时间
	private String state;//消费状态：1 成功；0 失败
	private String consumeValue;//消费金额,单位元
	private String extraData;//附加信息,可以为空
	private String gameServerZone;//用户此次消费所在游戏服务器分区
	private String consumeId;//消费订单号
	
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getUserNumId()
	{
		return userNumId;
	}
	public void setUserNumId(String userNumId)
	{
		this.userNumId = userNumId;
	}
	public String getGameId()
	{
		return gameId;
	}
	public void setGameId(String gameId)
	{
		this.gameId = gameId;
	}
	public long getReqtime()
	{
		return reqtime;
	}
	public void setReqtime(long reqtime)
	{
		this.reqtime = reqtime;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getConsumeValue()
	{
		return consumeValue;
	}
	public void setConsumeValue(String consumeValue)
	{
		this.consumeValue = consumeValue;
	}
	public String getExtraData()
	{
		return extraData;
	}
	public void setExtraData(String extraData)
	{
		this.extraData = extraData;
	}
	public String getGameServerZone()
	{
		return gameServerZone;
	}
	public void setGameServerZone(String gameServerZone)
	{
		this.gameServerZone = gameServerZone;
	}
	public String getConsumeId()
	{
		return consumeId;
	}
	public void setConsumeId(String consumeId)
	{
		this.consumeId = consumeId;
	}
}
