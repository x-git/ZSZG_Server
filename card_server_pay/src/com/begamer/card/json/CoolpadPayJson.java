package com.begamer.card.json;

public class CoolpadPayJson
{
	private String exorderno;//商户生成的订单号
	private String transid;//计费支付平台的交易流水号
	private String appid;//平台为商户应用分配的唯一代码
	private int waresid;//商品编号，目前默认为1
	private int feetype;//计费类型：0–消费型_应用传入价格
	private int money;//本次交易的金额，单位：分
	private int count;//本次购买的商品数量
	private int result;//交易结果：0–交易成功；1–交易失败
	private int transtype;//交易类型：0 – 交易；1 – 冲正
	private String transtime;//交易时间格式：yyyy-mm-dd hh24:mi:ss
	private String cpprivate;//商户私有信息
	private int paytype;//支付方式（该字段值后续可能会增加）0 –话费支付,1 - 充值卡,2 –游戏点卡,3 –银行卡,401 –支付宝,402 –财付通,5 –酷币,6 –酷派一键支付
	
	public String getExorderno()
	{
		return exorderno;
	}
	public void setExorderno(String exorderno)
	{
		this.exorderno = exorderno;
	}
	public String getTransid()
	{
		return transid;
	}
	public void setTransid(String transid)
	{
		this.transid = transid;
	}
	public String getAppid()
	{
		return appid;
	}
	public void setAppid(String appid)
	{
		this.appid = appid;
	}
	public int getWaresid()
	{
		return waresid;
	}
	public void setWaresid(int waresid)
	{
		this.waresid = waresid;
	}
	public int getFeetype()
	{
		return feetype;
	}
	public void setFeetype(int feetype)
	{
		this.feetype = feetype;
	}
	public int getMoney()
	{
		return money;
	}
	public void setMoney(int money)
	{
		this.money = money;
	}
	public int getCount()
	{
		return count;
	}
	public void setCount(int count)
	{
		this.count = count;
	}
	public int getResult()
	{
		return result;
	}
	public void setResult(int result)
	{
		this.result = result;
	}
	public int getTranstype()
	{
		return transtype;
	}
	public void setTranstype(int transtype)
	{
		this.transtype = transtype;
	}
	public String getTranstime()
	{
		return transtime;
	}
	public void setTranstime(String transtime)
	{
		this.transtime = transtime;
	}
	public int getPaytype()
	{
		return paytype;
	}
	public void setPaytype(int paytype)
	{
		this.paytype = paytype;
	}
	public String getCpprivate()
	{
		return cpprivate;
	}
	public void setCpprivate(String cpprivate)
	{
		this.cpprivate = cpprivate;
	}
}
