package com.begamer.card.common.dao.hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.log.DbErrorLogger;
import com.begamer.card.model.pojo.PayInfo;
import com.begamer.card.model.pojo.PayOrder;

public class CommDao extends HibernateDaoSupport
{
	private static final Logger dbLogger=DbErrorLogger.logger;
	
	public void save(PayInfo pi)
	{
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		try
		{
			session.save(pi);
			ts.commit();
		}
		catch (Exception e)
		{
			ts.rollback();
			dbLogger.error("保存消费信息出错", e);
		}
		finally
		{
			session.close();
		}
	}
	
	public void saveOrUpdate(PayOrder po)
	{
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		try
		{
			session.saveOrUpdate(po);
			ts.commit();
		}
		catch (Exception e)
		{
			ts.rollback();
			dbLogger.error("保存订单信息出错", e);
		}
		finally
		{
			session.close();
		}
	}
	
	@SuppressWarnings({"unchecked"})
	public PayOrder getPayOrder(int orderId)
	{
		Session session=getSession();
		try
		{
			String hql="from PayOrder p where p.id="+orderId;
			Query query=session.createQuery(hql);
			List<PayOrder> list=query.list();
			if(list!=null && list.size()>0)
			{
				return list.get(0);
			}
		}
		catch (Exception e)
		{
			dbLogger.error("更新订单信息出错", e);
		}
		finally
		{
			session.close();
		}
		return null;
	}
}
