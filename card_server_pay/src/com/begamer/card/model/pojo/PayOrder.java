package com.begamer.card.model.pojo;

public class PayOrder
{
	private int id;
	private String consumValue;
	private String extra;
	private String serverId;
	private String playerId;
	private String userId;
	private String reqTime;
	private int status;//0未支付,1已支付
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getConsumValue()
	{
		return consumValue;
	}
	public void setConsumValue(String consumValue)
	{
		this.consumValue = consumValue;
	}
	public String getExtra()
	{
		return extra;
	}
	public void setExtra(String extra)
	{
		this.extra = extra;
	}
	public String getServerId()
	{
		return serverId;
	}
	public void setServerId(String serverId)
	{
		this.serverId = serverId;
	}
	public String getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(String playerId)
	{
		this.playerId = playerId;
	}
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getReqTime()
	{
		return reqTime;
	}
	public void setReqTime(String reqTime)
	{
		this.reqTime = reqTime;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
}
