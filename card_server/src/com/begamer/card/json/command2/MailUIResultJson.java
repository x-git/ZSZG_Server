package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.MailUIResultElement;

public class MailUIResultJson extends ErrorJson
{
	//==未读邮件个数==//
	public int ur;
	//==邮件名字-发件人-发件时间(yyyy-MM-dd HH:ss)-是否新邮件(新邮件标识为1)==//
	public List<MailUIResultElement> ms;
	
	public int getUr()
	{
		return ur;
	}
	public void setUr(int ur)
	{
		this.ur = ur;
	}
	public List<MailUIResultElement> getMs()
	{
		return ms;
	}
	public void setMs(List<MailUIResultElement> ms)
	{
		this.ms = ms;
	}
}
