package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class CornucopiaResultJson extends ErrorJson
{
	public int id;//已经聚宝到第几个聚宝盆，若之前未聚宝，则为0
	public int time;//距离活动结束的时间  若活动结束则剩余时间为0，如果活动没有时间限制为-1
	public int crestalPay;//活动期间充值的水晶
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getCrestalPay() {
		return crestalPay;
	}

	public void setCrestalPay(int crestalPay) {
		this.crestalPay = crestalPay;
	}
	
}
