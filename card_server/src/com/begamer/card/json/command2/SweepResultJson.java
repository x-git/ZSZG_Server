package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class SweepResultJson extends ErrorJson
{
	//战前
	public int lv0;//军团等级
	public List<String> cs0;//卡组信息
	public int ce0;
	//战后
	public int lv1;
	public List<String> cs1;//角色卡,格式:cardId-level-curExp-maxExp-hp-atk-def
	public int ce1;
	
	public List<SweepCardJson> sweepInfo;//扫荡之后的军团，人物以及掉落物品
	public int power;//扫荡一次消耗的体力值
	public int entryTimes;//可挑战次数
	public int itemNum;//扫荡券个数
	public int sweepTimes;//可连续挑战次数
	
	public int power0;//升级前体力值
	public int power1;//升级后体力值
	
	//解锁模块    id-是否解锁(0未解锁，1解锁)
	public String [] s;
	
	public int getLv0() {
		return lv0;
	}
	public void setLv0(int lv0) {
		this.lv0 = lv0;
	}
	public List<String> getCs0() {
		return cs0;
	}
	public void setCs0(List<String> cs0) {
		this.cs0 = cs0;
	}
	public int getLv1() {
		return lv1;
	}
	public void setLv1(int lv1) {
		this.lv1 = lv1;
	}
	public List<String> getCs1() {
		return cs1;
	}
	public void setCs1(List<String> cs1) {
		this.cs1 = cs1;
	}
	public List<SweepCardJson> getSweepInfo() {
		return sweepInfo;
	}
	public void setSweepInfo(List<SweepCardJson> sweepInfo) {
		this.sweepInfo = sweepInfo;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getEntryTimes() {
		return entryTimes;
	}
	public void setEntryTimes(int entryTimes) {
		this.entryTimes = entryTimes;
	}
	public int getItemNum() {
		return itemNum;
	}
	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}
	public int getSweepTimes() {
		return sweepTimes;
	}
	public void setSweepTimes(int sweepTimes) {
		this.sweepTimes = sweepTimes;
	}
	public int getPower0() {
		return power0;
	}
	public void setPower0(int power0) {
		this.power0 = power0;
	}
	public int getPower1() {
		return power1;
	}
	public void setPower1(int power1) {
		this.power1 = power1;
	}
	public String[] getS() {
		return s;
	}
	public void setS(String[] s) {
		this.s = s;
	}
	public int getCe0() {
		return ce0;
	}
	public void setCe0(int ce0) {
		this.ce0 = ce0;
	}
	public int getCe1() {
		return ce1;
	}
	public void setCe1(int ce1) {
		this.ce1 = ce1;
	}
	
	
	
	
}
