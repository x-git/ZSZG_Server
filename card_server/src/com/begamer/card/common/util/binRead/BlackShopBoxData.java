package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class BlackShopBoxData implements PropertyReader
{

	public int number;
	public int viplevel;
	public int costtype;
	public int cost;
	
	private static HashMap<Integer, BlackShopBoxData> data =new HashMap<Integer, BlackShopBoxData>();
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData() {
		data.clear();
	}
	
	public static Integer getShopBoxNum()
	{
		int i=0;
		for(BlackShopBoxData bsData:data.values())
		{
			if(bsData.cost==0)
			{
				i=i+1;
			}
		}
		return i;
	}
	public static Integer getDataSize()
	{
		int i=0;
		for(BlackShopBoxData d:data.values())
		{
			if(d.cost>0)
			{
				i++;
			}
		}
		return i;
	}
	
	public static BlackShopBoxData getBlackShopBoxData(int index)
	{
		return data.get(index);
	}
}
