package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class FriendOddData implements PropertyReader
{
	public int level;
	public float number;
	
	private static HashMap<Integer, FriendOddData> data=new HashMap<Integer, FriendOddData>();
	
	@Override
	public void addData()
	{
		data.put(level, this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static FriendOddData getData(int level)
	{
		return data.get(level);
	}
}
