package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class SevenDaysData implements PropertyReader
{
	public int days;
	public List<String> items;
	
	private static HashMap<Integer, SevenDaysData> data =new HashMap<Integer, SevenDaysData>();
	@Override
	public void addData()
	{
		data.put(days, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location =0;
		days =StringUtil.getInt(ss[location]);
		items =new ArrayList<String>();
		for(int i=0;i<6;i++)
		{
			location =1+i*2;
			int itemType =StringUtil.getInt(ss[location]);
			String itemId =StringUtil.getString(ss[location+1]);
			if(itemType !=0)
			{
				String item =itemType+"-"+itemId;
				items.add(item);
			}
		}
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
	}

	public static SevenDaysData getSevenDaysData(int day)
	{
		return data.get(day);
	}
}
