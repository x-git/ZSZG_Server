package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MazeSkillDropData implements PropertyReader 
{
	/**编号**/
	public int id;
	/**技能掉落类型**/
	public int droptypeID;
	/**物品类型**/
	public int type;
	/**技能id**/
	public String skillID;
	/**几率**/
	public int probability;
	
	private static HashMap<Integer, MazeSkillDropData> data =new HashMap<Integer, MazeSkillDropData>();
	@Override
	public void addData() 
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) 
	{

	}

	@Override
	public void resetData() 
	{
		data.clear();
	}
	public static MazeSkillDropData getMazeSkillDropData(int msdId)
	{
		return data.get(msdId);
	}
	
	/**根据主动技能类型序列获取所有符合条件的dataList**/
	public static List<MazeSkillDropData> getRandomMazeSkillDropData(int dropId)
	{
		List<MazeSkillDropData> list =new ArrayList<MazeSkillDropData>();
		for(MazeSkillDropData msdData : data.values())
		{
			if(msdData.droptypeID ==dropId)
			{
				list.add(msdData);
			}
		}
		return list;
	}
}
