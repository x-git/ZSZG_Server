package com.begamer.card.model.pojo;

import com.begamer.card.common.util.StringUtil;

public class TimeMail {
	private int id;
	private String title;
	private String content;
	private String reward1;// 格式:type&id&number
	private String reward2;
	private String reward3;
	private String reward4;
	private String reward5;
	private String reward6;
	private int gold;
	private int crystal;
	private int runeNum;
	private int power;
	private int friendNum;
	private int state;// 1已开启,0未开启
	private String sendTime;// 格式HH:mm
	private String startDay;
	private String endDay;
	private int deleteTime;//删除邮件时间,格式mm
	
	private String lastSendDate;//上次发送日期,
	//private int canRemove;//1可删,0不可删

	public static TimeMail createMail(String title,String content,String reward1,String reward2,String reward3,String reward4,String reward5,String reward6,int gold,int crystal,int runeNum,int power,int friendNum,String startDay,String endDay, String sendTime,int deleteTime)
	{
		TimeMail mail=new TimeMail();
		mail.setTitle(title);
		mail.setContent(content);
		mail.setReward1(reward1);
		mail.setReward2(reward2);
		mail.setReward3(reward3);
		mail.setReward4(reward4);
		mail.setReward5(reward5);
		mail.setReward6(reward6);
		mail.setGold(gold);
		mail.setCrystal(crystal);
		mail.setRuneNum(runeNum);
		mail.setPower(power);
		mail.setFriendNum(friendNum);
		mail.setState(0);
		mail.setStartDay(startDay);
		mail.setEndDay(endDay);
		mail.setSendTime(sendTime);
		mail.setDeleteTime(deleteTime);
		return mail;
	}
	
	/**
	 * 判断定时邮件是否有效
	 * @param curDate
	 * @return -1数据无效,1还未到日期,0日期中
	 */
	public int valid(long curTime)
	{
		String curDate=StringUtil.getDate(curTime);
		if(startDay.compareTo(endDay)>0 || curDate.compareTo(endDay)>0)
		{
			return -1;
		}
		if(curDate.compareTo(startDay)<0)
		{
			return 1;
		}
		return 0;
	}
	
	/**
	 * 判断当前时间是否可发送
	 * @param curTime
	 * @return
	 */
	public boolean canSend(long curTimeStamp)
	{
		String curDate=StringUtil.getDate(curTimeStamp);
		String curTime=StringUtil.getTime2(curTimeStamp);
		
		long timeStamp=StringUtil.getTimeStamp(curDate+" "+sendTime+":00")+5*60*1000;
		if(curTime.compareTo(sendTime)>=0 && timeStamp-curTimeStamp>=0)
		{
			if(lastSendDate==null || "".equals(lastSendDate) || curDate.compareTo(lastSendDate)>0)
			{
				return true;
			}
		}
		return false;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getReward1()
	{
		return reward1;
	}

	public void setReward1(String reward1)
	{
		this.reward1 = reward1;
	}

	public String getReward2()
	{
		return reward2;
	}

	public void setReward2(String reward2)
	{
		this.reward2 = reward2;
	}

	public String getReward3()
	{
		return reward3;
	}

	public void setReward3(String reward3)
	{
		this.reward3 = reward3;
	}

	public String getReward4()
	{
		return reward4;
	}

	public void setReward4(String reward4)
	{
		this.reward4 = reward4;
	}

	public String getReward5()
	{
		return reward5;
	}

	public void setReward5(String reward5)
	{
		this.reward5 = reward5;
	}

	public String getReward6()
	{
		return reward6;
	}

	public void setReward6(String reward6)
	{
		this.reward6 = reward6;
	}

	public int getGold()
	{
		return gold;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getCrystal()
	{
		return crystal;
	}

	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}

	public int getRuneNum()
	{
		return runeNum;
	}

	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}

	public int getPower()
	{
		return power;
	}

	public void setPower(int power)
	{
		this.power = power;
	}

	public int getFriendNum()
	{
		return friendNum;
	}

	public void setFriendNum(int friendNum)
	{
		this.friendNum = friendNum;
	}

	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public String getSendTime()
	{
		return sendTime;
	}

	public void setSendTime(String sendTime)
	{
		this.sendTime = sendTime;
	}

	public String getStartDay()
	{
		return startDay;
	}

	public void setStartDay(String startDay)
	{
		this.startDay = startDay;
	}

	public String getEndDay()
	{
		return endDay;
	}

	public void setEndDay(String endDay)
	{
		this.endDay = endDay;
	}

	public String getLastSendDate()
	{
		return lastSendDate;
	}

	public void setLastSendDate(long curTimeStamp)
	{
		this.lastSendDate = StringUtil.getDate(curTimeStamp);
	}

	public int getDeleteTime()
	{
		return deleteTime;
	}

	public void setDeleteTime(int deleteTime)
	{
		this.deleteTime = deleteTime;
	}

}
