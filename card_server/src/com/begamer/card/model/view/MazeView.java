package com.begamer.card.model.view;

public class MazeView {
	/** *****迷宫-位置-次数-付费标识(0未付，1已付)，迷宫-位置-次数-付费标识**** */

	private int id;
	private String name;
	private String scene;
	private int num;
	private String pay;

	public static MazeView createMazeView(int id,String name,String scene,int num,String pay)
	{
		MazeView mazeView = new MazeView();
		mazeView.setId(id);
		mazeView.setName(name);
		mazeView.setScene(scene);
		mazeView.setNum(num);
		mazeView.setPay(pay);
		return mazeView;
	}
	
	
	public int getId()
	{
		return id;
	}


	public void setId(int id)
	{
		this.id = id;
	}


	public String getPay()
	{
		return pay;
	}

	public void setPay(String pay)
	{
		this.pay = pay;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getScene()
	{
		return scene;
	}

	public void setScene(String scene)
	{
		this.scene = scene;
	}

	public int getNum()
	{
		return num;
	}

	public void setNum(int num)
	{
		this.num = num;
	}

}
